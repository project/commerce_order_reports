<?php

/**
 * @file
 * Rules integration for Commerce Order Reports.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_order_reports_rules_action_info() {
  $actions = array();

  $actions['commerce_order_reports_generate'] = array(
    'label' => t('Create order reports'),
    'group' => t('Commerce Order Reports'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
      'report_type' => array(
        'type' => 'list<text>',
        'label' => t('Report Type'),
        'optional' => TRUE,
        'options list' => 'commerce_order_reports_order_report_options',
        'default mode' => 'input',
      ),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function commerce_order_reports_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_order_reports_order_has_reports'] = array(
    'label' => t('Order has order reports'),
    'group' => t('Commerce Order Reports'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
      'report_types' => array(
        'type' => 'text',
        'label' => t('Report types'),
        'optional' => TRUE,
        'options list' => 'commerce_order_reports_order_report_options',
      ),
    ),
  );

  return $conditions;
}
