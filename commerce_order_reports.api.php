<?php

/**
 * @file
 * Documentation and examples for hooks provided by the Order Reports module.
 */

/**
 * Defines the types of order reports provided by a module.
 *
 * @return \CommerceOrderReportsTypeInterface[]
 *   A list of order report types.
 */
function hook_commerce_order_reports_report_types() {
  return array(
    new MyReportType(),
    new ExcellentReportType(),
  );
}

/**
 * Alter the types of order reports provided by a module.
 *
 * This can be used to remove reports, like the base/default report. Do not
 * use this function to add new reports, this should always be done in
 * hook_commerce_order_reports_report_types() so proper validation can take
 * place.
 *
 * @param CommerceOrderReport[] $report_types
 *   A list of commerce order report types.
 *
 * @see hook_commerce_order_reports_report_types()
 */
function hook_commerce_order_reports_report_types_alter(array &$report_types) {
  $report_types = array_filter($report_types, function (CommerceOrderReportsTypeInterface $report_type) {
    return $report_type->getType() !== 'commerce_order_report';
  });
}
