<?php

/**
 * @file
 * The controller for the order entity containing the CRUD operations.
 */

/**
 * The controller for the order entity containing the CRUD operations.
 */
class CommerceOrderReportsEntityController extends DrupalCommerceEntityController {

  /**
   * Create a default order.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return object
   *   An order report object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array(
      'report_id' => NULL,
      'order_id' => 0,
      'data' => array(),
      'created' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  /**
   * Saves an order report.
   *
   * @param object $report
   *   The full commerce_order_reports object to save.
   * @param \DatabaseTransaction $transaction
   *   An optional transaction object.
   *
   * @return int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   *
   * @throws \Exception
   *   If unable to save the entity.
   */
  public function save($report, DatabaseTransaction $transaction = NULL) {
    if ($transaction === NULL) {
      $transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      // Determine if we will be inserting a new order.
      $report->is_new = empty($report->report_id);

      // Set the timestamp fields.
      if ($report->is_new && empty($report->created)) {
        $report->created = REQUEST_TIME;
      }
      elseif ($report->created === '') {
        // Otherwise if the report is not new but comes from an entity_create()
        // or similar function call that initializes the created timestamp and
        // hostname values to empty strings, unset them to prevent destroying
        // existing data in those properties on update.
        unset($report->created);
      }

      $report->data = json_encode($report->data);

      return parent::save($report, $transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }

  /**
   * Unserializes the data property of loaded orders.
   *
   * @param mixed $queried_reports
   *   An array of commerce_order_reports.
   * @param int|bool $revision_id
   *   An entity revision ID.
   */
  public function attachLoad(&$queried_reports, $revision_id = FALSE) {
    foreach (array_keys($queried_reports) as $report_id) {
      $queried_reports[$report_id]->data = json_decode($queried_reports[$report_id]->data);
    }

    parent::attachLoad($queried_reports, $revision_id);
  }

}
