<?php

/**
 * @file
 * Export Drupal Commerce orders to Views.
 */

/**
 * Implements hook_views_data().
 */
function commerce_order_reports_views_data() {
  $data = array();

  $data['commerce_order_reports']['table']['group'] = t('Order Report Entry');

  $data['commerce_order_reports']['table']['base'] = array(
    'field' => 'report_id',
    'title' => t('Commerce Order Report'),
    'help' => t('Report of order placed in the store.'),
    // TODO How does this work? Does the report ID go to through
    // commerce_order_access? 'cause that won't work.
    'access query tag' => 'commerce_order_access',
  );
  $data['commerce_order_reports']['table']['entity type'] = 'commerce_order_reports';

  $data['commerce_order_reports']['table']['default_relationship'] = array(
    'commerce_order' => array(
      'table' => 'commerce_order',
      'field' => 'order_id',
    ),
  );

  $data['commerce_order_reports']['report_id'] = array(
    'title' => t('Report ID'),
    'help' => t('The unique identifier of the report.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['commerce_order_reports']['type'] = array(
    'title' => t('Type'),
    'help' => t('The type of the report.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the order ID.
  $data['commerce_order_reports']['order_id'] = array(
    'title' => t('Order ID', array(), array('context' => 'a drupal commerce order')),
    'help' => t('The unique internal identifier of the order.'),
    'field' => array(
      'handler' => 'commerce_order_handler_field_order',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'commerce_order_handler_argument_order_order_id',
      'name field' => 'order_number',
      'numeric' => TRUE,
      'validate type' => 'order_id',
    ),
    'relationship' => array(
      'title' => t('Commerce Order'),
      'help' => t('Relate this order to the Commerce Order'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'field' => 'order_id',
      'label' => t("Report's order"),
    ),
  );

  $data['commerce_order_reports']['order_report_date'] = array(
    'title' => t('Order report date'),
    'help' => t('Field containing the date of the order, with the ability to filter on granularity.'),
    'real field' => 'created',
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );

  // Expose the created and changed timestamps.
  $data['commerce_order_reports']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the report was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['commerce_order_reports']['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['commerce_order_reports']['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['commerce_order_reports']['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['commerce_order_reports']['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['commerce_order_reports']['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['commerce_order_reports']['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  return $data;
}
