<?php

/**
 * @file
 * Form builders and their validate and submit handlers.
 */

/**
 * Allows batch generation of reports.
 */
function commerce_order_reports_generation_form($form, &$form_state) {

  // Steps are 'start' and 'confirm'.
  $form_state['step'] = $form_state['step'] ?? 'start';

  $form_function = 'commerce_order_reports_generation_form_' . $form_state['step'];
  if (function_exists($form_function)) {
    $form = $form_function($form, $form_state);
  }
  return $form;
}

/**
 * Build the first step of the report generation form.
 *
 * Allows selecting which type of report to generate.
 */
function commerce_order_reports_generation_form_start($form, &$form_state) {
  $form['description'] = array(
    '#markup' => '<p>' . t('Orders have reports created automatically when paid in full. To generate reports for old orders, select the type of report to create.') . '</p>',
  );

  $options = commerce_order_reports_order_report_options();
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Report Type'),
    '#description' => t('Choose a type of report to generate.'),
    '#options' => $options,
    '#default_value' => $form_state['values']['type'] ?? $form_state['type'] ?? '',
    '#required' => TRUE,
  );

  $form['statuses'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Order Statuses'),
    '#description' => t('Reports will only be generated for orders in these statuses.'),
    '#default_value' => $form_state['values']['type'] ?? $form_state['statuses'] ?? [],
    '#options' => commerce_order_status_options_list(),
    '#size' => 10,
  );

  $form['update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update'),
    '#description' => t('To update, all existing reports are deleted and re-generated. This may change some reports which were snapshots from when the order completed and will now be snapshots of the order currently.'),
    '#default_value' => $form_state['values']['update'] ?? $form_state['update'] ?? '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Reports'),
  );

  return $form;
}

/**
 * Second step of the report generation form.
 *
 * Requires the user to confirm that they want to generate reports.
 */
function commerce_order_reports_generation_form_confirm($form, &$form_state) {
  $form_state['type'] = $form_state['values']['type'];
  $form_state['update'] = $form_state['values']['update'];
  $form_state['statuses'] = $form_state['values']['statuses'];

  $report_type = commerce_order_reports_get_report_type($form_state['type']);
  if ($report_type !== NULL) {
    if ($form_state['update']) {
      $form['confirmation'] = array(
        '#markup' => '<p>' . t('This will delete reports for @num_existing_orders orders and then generate the @report_type for all orders. Continue?', array(
          '@report_type' => $report_type->getLabel(),
          '@num_existing_orders' => commerce_order_reports_num_orders_with_report($report_type, $form_state['statuses']),
        )) . '</p>',
      );
    }
    else {
      $form['confirmation'] = array(
        '#markup' => '<p>' . t('This will generate the @report_type for @num_orders orders. Continue?', array(
          '@report_type' => $report_type->getLabel(),
          '@num_orders' => commerce_order_reports_num_orders_without_report($report_type, $form_state['statuses']),
        )) . '</p>',
      );
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Generate Reports'),
    );
  }

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('commerce_order_reports_generation_form_cancel'),
  );

  return $form;
}

/**
 * Callback to cancel generating reports.
 */
function commerce_order_reports_generation_form_cancel($form, &$form_state) {
  $form_state['step'] = 'start';
  $form_state['rebuild'] = TRUE;
}

/**
 * Start the batch api for order generation.
 */
function commerce_order_reports_generation_form_submit($form, &$form_state) {
  if ($form_state['step'] !== 'confirm') {
    $form_state['step'] = 'confirm';
    $form_state['rebuild'] = TRUE;
    return;
  }

  $update = $form_state['update'] ?? FALSE;
  if ($update) {
    $operations[] = array(
      'commerce_order_reports_batch_delete',
      array(
        $form_state['type'],
        $form_state['statuses'],
      ),
    );
  }
  $operations[] = array(
    'commerce_order_reports_batch_generation',
    array(
      $form_state['type'],
      $form_state['statuses'],
    ),
  );

  $batch = array(
    'operations' => $operations,
    'file' => drupal_get_path('module', 'commerce_order_reports') . '/forms/commerce_order_reports.admin.inc',
  );

  batch_set($batch);
}

/**
 * Batch operation to delete existing reports before re-generating new ones.
 *
 * @param string $report_type
 *   The name of a report type.
 * @param string[] $statuses
 *   A list of order statuses to process reports for. Empty to allow any status.
 * @param array $context
 *   The Batch API context.
 */
function commerce_order_reports_batch_delete($report_type, array $statuses, array &$context) {
  $report_type = commerce_order_reports_get_report_type($report_type);
  // Get the total number of reports to process on the first run.
  $context['sandbox']['num_reports'] = $context['sandbox']['num_reports'] ?? commerce_order_reports_num_orders_with_report($report_type, $statuses);
  // Initialize the count of orders processed on the first run.
  $context['sandbox']['orders_processed'] = $context['sandbox']['orders_processed'] ?? 0;
  $batch_size = 25;

  // Delete reports for 25 orders at a time.
  foreach (commerce_order_reports_orders_with_report($report_type, $batch_size, NULL, $statuses) as $order_id) {
    commerce_order_reports_delete_reports($report_type, $order_id);
  }

  $context['sandbox']['orders_processed'] += $batch_size;
  $context['finished'] = $context['sandbox']['orders_processed'] / $context['sandbox']['num_reports'];
}

/**
 * Batch callback to generate reports.
 *
 * @param string $report_type
 *   The name of a report type.
 * @param string[] $statuses
 *   A list of order statuses to process reports for. Empty to allow any status.
 * @param array $context
 *   The Batch API context.
 */
function commerce_order_reports_batch_generation($report_type, array $statuses, array &$context) {
  $report_type = commerce_order_reports_get_report_type($report_type);
  // Get the total number of reports to proce
  $context['sandbox']['num_reports'] = $context['sandbox']['num_reports'] ?? commerce_order_reports_num_orders_without_report($report_type, $statuses);
  // Initialize the count of orders processed on the first run.
  $context['sandbox']['orders_processed'] = $context['sandbox']['orders_processed'] ?? 0;
  // Since we're loading orders with no report, offset is always 0; the ones we
  // generated reports for in previous pages of the batch are not returned by
  // the query.
  $offset = 0;
  $batch_size = 25;

  $last_order = $context['sandbox']['last_order'] ?? 0;

  $ids = commerce_order_reports_orders_without_report($report_type, $batch_size, $offset, $statuses, $last_order);
  foreach (commerce_order_load_multiple($ids) as $order) {
    commerce_order_reports_generate($order, [$report_type]);
  }

  $context['sandbox']['orders_processed'] += $batch_size;
  if ($ids) {
    // Remember the last order so we don't try processing it again.
    $context['sandbox']['last_order'] = end($ids);
    $context['finished'] = $context['sandbox']['orders_processed'] / $context['sandbox']['num_reports'];
  }
  else {
    // If we hadn't found any more orders we're done processing.
    $context['finished'] = 1;
  }
}
