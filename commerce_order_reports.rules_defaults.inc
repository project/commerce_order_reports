<?php

/**
 * @file
 * Default rules configurations for Commerce Order Reports.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_order_reports_default_rules_configuration() {
  $configs = array();
  $rule = rules_import('{
    "rules_commerce_order_report_create_report_record" : {
      "LABEL" : "Commerce Order Report: create report records",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "TAGS" : [ "Commerce Order Report" ],
      "REQUIRES" : [ "commerce_order_reports", "commerce_payment", "commerce_checkout"],
      "ON" : { "commerce_payment_order_paid_in_full" : [] },
      "DO" : [{
        "commerce_order_reports_generate" : {
          "commerce_order" : [ "commerce_order" ],
          "report_type" : { "value" : [] }
        }
      }]
    }
  }');

  // If commerce_checkout is not enabled then
  // 'commerce_payment_order_paid_in_full' will not exist,
  // causing rules_import() to return FALSE. If FALSE, a PDOException will
  // occur when it tries to insert the "new" rule.
  if ($rule) {
    $configs['rules_commerce_order_report_create_report_record'] = $rule;
  }

  return $configs;
}
