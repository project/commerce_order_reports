<?php

/**
 * A default order report type.
 */
class CommerceOrderReport extends CommerceOrderReportBase {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return 'commerce_order_report';
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return t('Default Order Report');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields['order_report_tax_amount'] = array(
      'type' => 'commerce_price',
    );
    $fields['order_report_shipping_amount'] = array(
      'type' => 'commerce_price',
    );
    $fields['order_report_total_amount'] = array(
      'type' => 'commerce_price',
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function instances() {
    $instances['order_report_tax_amount'] = array(
      'label' => t('Tax Amount'),
    );
    $instances['order_report_shipping_amount'] = array(
      'label' => t('Shipping Amount'),
    );
    $instances['order_report_total_amount'] = array(
      'label' => t('Total Amount'),
    );
    return $instances;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($order) {
    $this->createFromValues($order, array(
      'order_report_tax_amount' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'amount' => commerce_order_reports_order_tax_total($order),
            'currency_code' => commerce_order_reports_order_currency($order),
          ),
        ),
      ),
      'order_report_shipping_amount' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'amount' => commerce_order_reports_order_shipping_total($order),
            'currency_code' => commerce_order_reports_order_currency($order),
          ),
        ),
      ),
      'order_report_total_amount' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'amount' => commerce_order_reports_order_base_total($order),
            'currency_code' => commerce_order_reports_order_currency($order),
          ),
        ),
      ),
    ));
  }

}
