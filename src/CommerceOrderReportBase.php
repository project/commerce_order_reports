<?php

/**
 * Base class for order report types.
 */
abstract class CommerceOrderReportBase implements CommerceOrderReportsTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function createFields() {
    $fields = $this->fields();
    $fields = array_map(function ($field, $field_name) {
      $field['field_name'] = $field_name;
      return $field;
    }, $fields, array_keys($fields));
    foreach ($fields as $field) {
      $existing_field = field_info_field($field['field_name']);
      if (!$existing_field) {
        field_create_field($field);
      }
    }

    $instances = $this->instances();
    $instances = array_map(function ($instance, $field_name) {
      $instance['field_name'] = $field_name;
      $instance['entity_type'] = 'commerce_order_reports';
      $instance['bundle'] = $this->getType();
      return $instance;
    }, $instances, array_keys($instances));
    foreach ($instances as $instance) {
      // For convenience, commerce_price fields receive default settings. Since
      // this actually creates the instance we can't just use it for defaults
      // and override with the report type's choices.
      $field = field_info_field($instance['field_name']);
      if ($field['type'] === 'commerce_price') {
        commerce_price_create_instance($instance['field_name'], $instance['entity_type'], $instance['bundle'], $instance['label'], 0, 'calculated_sell_price');
        continue;
      }

      $existing_instance = field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
      if (!$existing_instance) {
        field_create_instance($instance);
      }

    }
  }

  /**
   * Create an order report with the specified values.
   *
   * @param object $order
   *   The commerce_order entity for the report.
   * @param array $values
   *   An associative array of values for the report.
   *
   * @throws \Exception
   *   If unable to save the report.
   */
  public function createFromValues($order, array $values = array()) {
    /** @var \CommerceOrderReportsEntityController $report_controller */
    $report_controller = entity_get_controller('commerce_order_reports');
    $report = $report_controller->create($values + array(
      'order_id' => $order->order_id,
      'type' => static::getType(),
      'created' => $order->placed ?: $order->changed,
    ));
    $report_controller->save($report);
  }

}
