<?php

/**
 * Defines a type of order report.
 */
interface CommerceOrderReportsTypeInterface {

  /**
   * Get the machine name of this type of report.
   *
   * @return string
   *   A string suitable for an entity bundle name, like "my_report_type".
   */
  public function getType();

  /**
   * Get the human-friendly label for this type of report.
   *
   * @return string
   *   A translated name like "My Report Type".
   */
  public function getLabel();

  /**
   * Get fields on this report.
   *
   * @return array[]
   *   An array of field definitions for field_create_field except field_name is
   *   filled in automatically.
   *
   *   The keys of this array must be the field name.
   */
  public function fields();

  /**
   * Get field instances on this report.
   *
   * @return array[]
   *   An array of field instances for field_create_instance except field_name,
   *   entity_type, and bundle are not required. They'll be filled in
   *   automatically.
   *
   *   The keys of this array must be the field name.
   */
  public function instances();

  /**
   * Creates fields and their instances for the report.
   *
   * You should call this method on your reports when installing your module and
   * update fields in regular update hooks if necessary.
   *
   * @see commerce_order_reports_install()
   */
  public function createFields();

  /**
   * Generate reports for an order.
   *
   * @param object $order
   *   A commerce_order object.
   *
   * @return object[]
   *   A list of created commerce_order_report entities.
   */
  public function generate($order);

}
