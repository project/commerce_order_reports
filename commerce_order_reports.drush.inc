<?php

/**
 * @file
 * Drush commands for Commerce Order Reports.
 */

/**
 * Implements hook_drush_command().
 */
function commerce_order_reports_drush_command() {
  $commands['commerce-order-reports-list-types'] = array(
    'description' => 'List installed types of order reports',
  );

  $commands['commerce-order-reports-generate-reports'] = array(
    'description' => 'Generate order reports',
    'arguments' => array(
      'type' => 'The type of report to generate. See commerce-order-reports-list-types.',
    ),
    'options' => array(
      'update' => 'Delete existing orders and re-generate them.',
    ),
  );

  return $commands;
}

/**
 * Print a list of installed report types.
 */
function drush_commerce_order_reports_list_types() {
  $types = commerce_order_reports_order_report_types();

  if (empty($types)) {
    drush_print(dt('There are no order report types installed.'));
    return;
  }

  drush_print(dt('Installed reports:'));
  drush_print(dt('Report Type | Name'));
  foreach ($types as $type) {
    drush_print(dt('@type | @name', array(
      '@type' => $type->getType(),
      '@name' => $type->getLabel(),
    )));
  }
}

/**
 * Generate reports for all orders.
 *
 * Allows updating reports by deleting existing ones and re-generating them.
 *
 * @param string $report_type
 *   The machine name of a report type.
 */
function drush_commerce_order_reports_generate_reports($report_type) {
  $type = commerce_order_reports_get_report_type($report_type);

  if (!$type) {
    drush_log('@type is not a valid report type. Check commerce-order-reports-list-types.', array('@type' => '@report_type'), 'error');
    return;
  }

  // TODO Check for update option.
  $update = drush_get_option('update', FALSE);
  // TODO Confirm the amounts to delete/re-create like in the admin form.
  $go = drush_confirm(dt('TODO Are you sure?'));

  if (!$go) {
    drush_print(dt('Aborted by the user.'));
    return;
  }

  // TODO Add command equivalent to commerce_order_reports_batch_generation().
}

