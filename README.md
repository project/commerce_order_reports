# Commerce Order Reports

This module aims to provide a solid reporting system for Drupal Commerce.

Reasoning for this module:
Commerce Reports is based on order creation date and not checkout. Technically
certain orders could be created a day before checkout is finished, or midnight
purchases.

Commerce Google Analytics triggers on checkout complete, therefore if you're
using Commerce Reports and Google Analytics you will have two different data
sets.

Reliable data from time of checkout. These are snapshots from checkout so they
are unaffected by changes to the orders.

## How it works

Commerce Order Reports creates a new entity that stores data about orders.

TODO These were removed from the base table. Add these back as fields.
* Base price (includes discounts and coupons)
* Shipping cost
* Tax amount

The module enables a rule which triggers on the event "Order paid in full". This
was chosen over Checkout Completed for admin-created and fulfilled orders.

## Creating new reports

New types of reports can be created by implementing an interface and some hooks.

1. Implement CommerceOrderReportsTypeInterface. For convenience, you may want to
   extend CommerceOrderReportBase.

   This class tells us about your report, sets up the fields your report uses,
   and generates the data for the report.

   See CommerceOrderReport for an example.

2. Create fields for your reports during your module's install. See
   `commerce_order_reports_install()` for an example. Note that if updates to
   field definitions are required you're on your own for that. Keep in mind that
   field names should be unique; try not to use names that will conflict with
   other modules or user-created fields.

3. Tell us about your report types with
   `hook_commerce_order_reports_report_types()`.

4. Use views to build a report using your report type.

## TO DO

[ ] Open.
[-] In progress.
[x] Complete.

- [-] Create hook to declare new report types.
  - [x] Document hook in api.php file.
  - [x] Maybe needs work on what the hook should return?
  - [ ] Views support needs aggregation-related information in the report type
    kinda like the D8 version.
- [x] Properly support types of reports (needs type in schema, maybe set type as
  the bundle key in entity info?)
- [x] Demonstrate adding fields to reports (currently working on it during
  install of test module.
  - [x] Provide an easier framework for adding fields to new reports?
- [x] Generate reports on order complete (original only made the default type).
- [x] Create the default report type.
- [x] Documentation for how to create new report types.
- [-] Allow generation of old reports.
  - [x] Batch.
  - [-] Drush.
- [x] Allow re-generation of reports.
  - [x] Delete existing reports.
- [x] Allow configuration of reports to use (ie, don't enable the default one if
  it's not helpful).
- [ ] Re-asses documentation. Is it good enough?
  - [ ] For a site builder to work with reports.
  - [ ] For a developer to create new types of reports.
- [ ] If a report type doesn't generate any reports for a certain order it'll
  always run when bulk generating reports.
- [ ] Support aggregation in views.
